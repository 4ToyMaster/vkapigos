package com.example.vlad.ex1;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    Button showBTN;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.main_activity_text);
        showBTN = findViewById(R.id.showBTN);

        db = getBaseContext().openOrCreateDatabase("ex1.db", MODE_PRIVATE, null);

        db.execSQL("CREATE TABLE IF NOT EXISTS albums(id INTEGER, title TEXT)");

        VKSdk.login(MainActivity.this, VKScope.FRIENDS, VKScope.GROUPS, VKScope.VIDEO);
        final VKRequest request =new VKRequest("video.getAlbums", VKParameters.from(VKApiConst.OWNER_ID, "-57876954"));
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                //textView.append(response.responseString);
                try {
                    JSONArray array = response.json.getJSONObject("response").getJSONArray("items");
                    for(int i=0; i< array.length(); i++){
                        String title = array.getJSONObject(i).get("title").toString();
                        title =  title.replace("'", "`");
                        int id =Integer.parseInt(array.getJSONObject(i).get("id").toString());
                        //textView.append(id+" : " +title +"; \n");
                        db.execSQL("INSERT INTO albums VALUES (" + id + ",'" + title + "');");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                super.onComplete(response);
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                super.attemptFailed(request, attemptNumber, totalAttempts);
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
            }

            @Override
            public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
                super.onProgress(progressType, bytesLoaded, bytesTotal);
            }
        });
    }

    public void onClickShowBTN(View view){
        Cursor query = db.rawQuery("SELECT * FROM albums", null);
        if (query.moveToFirst()){
            do{
                int id = query.getInt(0);
                String name = query.getString(1);
                textView.append("ID = "+ id + ", TITLE = "+ name +";\n");
            }
            while (query.moveToNext());
        }
        query.close();
    }
}
